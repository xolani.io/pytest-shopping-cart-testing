from typing import List


class ShoppingCart:
    def __init__(self):
        self.items: List[str] = []
        self.price_map_list: List[map] = []
        self.total_price : List[int] = []
        
    def add(self, item:str):
        self.items.append(item)
    
    def size(self) ->  int:
        return len(self.items)
    
    def get_items(self) -> List[str]:
        return self.items
    
    def add_item_map_to_list(self, name:str, price:int,) -> List:
        price_map = {name:price}
        self.price_map_list.append(price_map)
        self.total_price.append(price)
        return self.price_map_list
    
    def total_price_of_cart(self) -> int:
        return sum(self.total_price)
        