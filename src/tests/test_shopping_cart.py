from models.shopping_cart import ShoppingCart


def test_add_item_to_cart():
    my_cart = ShoppingCart()
    my_cart.add("1st_item")
    assert my_cart.size() == 1
    
def test_total_price_of_cart():
    my_cart = ShoppingCart()
    my_cart.add_item_map_to_list("sugar", 10) 
    my_cart.add_item_map_to_list("milk", 20) 
    my_cart.add_item_map_to_list("coffe", 30) 
    assert my_cart.total_price_of_cart() == 60